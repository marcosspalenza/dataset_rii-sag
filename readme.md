# Dataset Multidisciplinar de Respostas Curtas

Dados de 14 atividades textuais coletadas em inglês e português. Cada atividade é representada pela matriz com os vetores de características indexados e os rótulos de avaliação.

# Vetorização
O processo de vetorização foi realizado após um nível de pré-processamento com os seguintes módulos:

* Remoção tags HTML
* Remoção de pontuação e acentuação
* Remoção de *Stopwords*
* *Stemming*
* Análise em *N-Grams*

Após a padronização o processo de vetorização foi dado pelo Term-Frequency - TF com a contagem *a priori* das frequências dos termos identificados. Após a vetorização, o modelo de matriz utilizado para este *dataset* foi o *matrix market*.

Os vetores em *matrix market* foram gerados automaticamente através do [SciPy](https://docs.scipy.org/doc/scipy-0.19.1/reference/generated/scipy.io.mmwrite.html)

# Características
Os vetores de cada atividade são apresentados nos arquivos *.mtx*. Cada vetor indica uma resposta dada por um aluno para uma determinada atividade. Os documentos foram indexados após os métodos de padronização e representados em *matrix market*. A vetorização dos documentos do dataset é representado conforme o exemplo abaixo:

```
%%MatrixMarket matrix coordinate integer general
%
1278 1966 29170
1 261 1
1 321 1
1 384 1
1 445 1
1 490 1
1 616 1
1 619 1
1 732 2
1 741 1
1 792 1
1 838 1
1 961 1
1 1090 1
1 1095 1
1 1108 1
1 1208 3
1 1602 1
1 1612 1
1 1791 4
1 1792 1
1 1817 1
1 1949 1
2 170 1
2 344 1
2 382 1
2 426 2
2 445 1
2 470 1
2 616 1
2 739 1
2 768 1
[...]
```
Os números da primeira linha da matriz indicam respectivamente o número de respostas, o número de características e a quantidade de características encontradas nos vetores (elementos não-nulos).
Após isso, seguem o índice do documento, o índice da característica e o valor encontrado no documento.

# Classes
No arquivo *.class* da atividade temos o nome do documento e a classe ao qual ele foi associado. O documento *'1'* apresentado no exemplo anterior pode ser encontrado na primeira linha do documento:

```
2788 1
2789 3
2790 1
2791 1
2792 3
2793 2
2794 1
2795 2
2796 2
[...]
```

Nesse exemplo, o identificador do documento representa a linha com sua informação. Assim, na primeira linha do arquivo *.class* temos a informação relativa ao documento *'1'*. O documento na base de dados original se encontra como *2788* e a classe representada por ele foi rotulada como *1*.

> 2788 1

# Versão 
v1.0 Dataset RII-SAG